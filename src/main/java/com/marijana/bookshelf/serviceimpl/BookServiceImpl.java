package com.marijana.bookshelf.serviceimpl;

import java.util.List;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marijana.bookshelf.infrastructure.exceptions.ResourceNotFoundException;
import com.marijana.bookshelf.model.Book;
import com.marijana.bookshelf.repository.BookRepository;
import com.marijana.bookshelf.service.GenericService;


import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class BookServiceImpl implements GenericService<Book, Integer> {
	
	@Autowired
	BookRepository repository;

	@Override
	public Book findById(Integer id) {
		Book entity = repository.findById(id).orElseThrow(() -> {
			log.error("Resource Book with id {} is not found", id);
			return new ResourceNotFoundException("Resource Book not found");
		});
		
		return entity;
	}
	
	@Override
	public List<Book> findAll() {
		log.debug("Execute findAll Books");
		return repository.findAll();
	}

	@Override
	public Book create(Book entity) {
		log.debug("Execute create Book with parameters ", entity);
		Book persistedEntity = repository.save(entity);
		return persistedEntity;
	}

	@Override
	public Book update(Integer id, Book entity) {
		log.debug("Execute update Book with parameters {}", entity);
		Book persistedEntity = findById(id);
		
		persistedEntity.setTitle(entity.getTitle());
		persistedEntity.setDatePublished(entity.getDatePublished());
		persistedEntity.setEdition(entity.getEdition());
		persistedEntity.setNoPages(entity.getNoPages());
		persistedEntity.setAuthors(entity.getAuthors());
		persistedEntity.setPublisher(entity.getPublisher());
		persistedEntity.setCategory(entity.getCategory());
		persistedEntity.setFormats(entity.getFormats());

		return repository.saveAndFlush(persistedEntity);
	}

	@Override
	public void deleteById(Integer id) {
		log.debug("Execute deleteById Book with parameters {}", id);
		repository.deleteById(id);
	}

}
