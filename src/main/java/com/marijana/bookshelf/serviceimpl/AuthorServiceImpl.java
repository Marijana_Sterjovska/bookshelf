package com.marijana.bookshelf.serviceimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marijana.bookshelf.infrastructure.exceptions.ResourceNotFoundException;
import com.marijana.bookshelf.model.Author;
import com.marijana.bookshelf.repository.AuthorRepository;
import com.marijana.bookshelf.service.GenericService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class AuthorServiceImpl implements GenericService<Author, Integer> {
	@Autowired
	public AuthorRepository repository;

	@Override
	public Author findById(Integer id) {
		Author entity = repository.findById(id).orElseThrow(() -> {
			log.error("Resource Author with id {} is not found", id);
			return new ResourceNotFoundException("Resource Author not found");
		});

		return entity;
	}

	@Override
	public List<Author> findAll() {
		log.debug("Execute findAll Author");
		return repository.findAll();
	}

	@Override
	public Author create(Author entity) {
		log.debug("Execute create Author with parameters ", entity);
		Author persistedEntity = repository.save(entity);
		return persistedEntity;
	}

	@Override
	public Author update(Integer id, Author entity) {
		log.debug("Execute update Author with parameters {}", entity);
		Author persistedEntity = findById(id);
		persistedEntity.setFirstName(entity.getFirstName());
		persistedEntity.setLastName(entity.getLastName());
		return repository.saveAndFlush(persistedEntity);
	}

	@Override
	public void deleteById(Integer id) {
		log.debug("Execute deleteById Author with parameters {}", id);
		repository.deleteById(id);
	}
}
