package com.marijana.bookshelf.serviceimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.marijana.bookshelf.infrastructure.exceptions.ResourceNotFoundException;
import com.marijana.bookshelf.model.Format;
import com.marijana.bookshelf.repository.FormatRepository;
import com.marijana.bookshelf.service.GenericService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Transactional
public class FormatServiceImpl implements GenericService<Format, Integer> {

	@Autowired
	public FormatRepository repository;

	@Override
	public Format findById(Integer id) {
		Format entity = repository.findById(id).orElseThrow(() -> {
			log.error("Resource Format with id {} is not found", id);
			return new ResourceNotFoundException("Resource Format not found");
		});

		return entity;
	}

	@Override
	public List<Format> findAll() {
		log.debug("Execute findAll Format");
		return repository.findAll();
	}

	@Override
	public Format create(Format entity) {
		log.debug("Execute create Format with parameters ", entity);
		Format persistedEntity = repository.save(entity);
		return persistedEntity;
	}

	@Override
	public Format update(Integer id, Format entity) {
		log.debug("Execute update Format with parameters {}", entity);
		Format persistedEntity = findById(id);
		persistedEntity.setType(entity.getType());
		return repository.saveAndFlush(persistedEntity);
	}

	@Override
	public void deleteById(Integer id) {
		log.debug("Execute deleteById Format with parameters {}", id);
		repository.deleteById(id);
	}

}
