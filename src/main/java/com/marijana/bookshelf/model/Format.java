package com.marijana.bookshelf.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "formats")
public class Format {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "format_generator")
	@SequenceGenerator(name="format_generator", sequenceName = "format_seq")
	private Integer id;
	
	@NonNull
	@Column(name = "format_type", nullable = false)
	private String type;
	
	@ManyToMany(mappedBy = "formats", cascade = CascadeType.ALL, fetch = FetchType.LAZY )
	@JsonIgnoreProperties("formats")
	private Set<Book>books;
}
