package com.marijana.bookshelf.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "books")
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_Sequence")
    @SequenceGenerator(name="book_Sequence", sequenceName = "book_seq")
 	private Integer id;

	@NonNull
	@Column(name = "title", nullable = false)
	private String title;
	
	@NonNull
	@Column(name = "date_published", nullable = false)
	private String datePublished;
	
	@NonNull
	@Column(name="edition", nullable = false)
	private String edition;
	
	@NonNull
	@Column(name="isbn", nullable = false)
	private String isbn;
	
	@NonNull
	@Column(name="no_pages", nullable = false)
	private Integer noPages;
	
	@NonNull
	@JsonIgnoreProperties("books")
	@ManyToMany(cascade = CascadeType.ALL )
	@JoinTable(name="books_authors", 
	joinColumns = @JoinColumn(name = "book_id"),
	inverseJoinColumns = @JoinColumn(name = "author_id"))
	private List <Author> authors;
	

	@NonNull
	@JsonIgnoreProperties("books")
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="publisher_id")
	private Publisher publisher;
	
	@NonNull
	@JsonIgnoreProperties("books")
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name ="category_id")
	private Category category;
	
	@NonNull
	@JsonIgnoreProperties("books")
	@ManyToMany(cascade = CascadeType.ALL )
	@JoinTable(name="books_formats", 
		joinColumns = @JoinColumn(name = "book_id"),
		inverseJoinColumns = @JoinColumn(name = "format_id"))
	private List<Format> formats;

}
