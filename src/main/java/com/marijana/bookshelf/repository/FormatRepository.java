package com.marijana.bookshelf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marijana.bookshelf.model.Format;

@Repository
public interface FormatRepository extends JpaRepository<Format, Integer> {
}
