package com.marijana.bookshelf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marijana.bookshelf.model.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Integer> {
}
