package com.marijana.bookshelf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.marijana.bookshelf.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> { 
}
