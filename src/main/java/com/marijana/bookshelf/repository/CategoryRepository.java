package com.marijana.bookshelf.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.marijana.bookshelf.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> { 
}

