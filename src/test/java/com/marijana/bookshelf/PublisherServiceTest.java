package com.marijana.bookshelf;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.marijana.bookshelf.model.Publisher;
import com.marijana.bookshelf.repository.PublisherRepository;
import com.marijana.bookshelf.serviceimpl.PublisherServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PublisherServiceTest {
	@InjectMocks
	private PublisherServiceImpl service;
	
	@Mock
	private PublisherRepository repository;
	
	public void findAllPublishersTest(){
		Mockito.when(repository.findAll()). thenReturn(Arrays.asList(
				new Publisher("O'Reilly Media, Inc"),
				new Publisher("SAMS")
		));
		
		List<Publisher>allPublishers= service.findAll();
		assertEquals("O'Reilly Media, Inc", allPublishers.get(0).getName());
		assertEquals("SAMS", allPublishers.get(1).getName());	
	}
	
	@Test
	public void findPublisherbyId() {
		Mockito.when(repository.findById(1)).thenReturn(
				Optional.of(new Publisher("O'Reilly Media, Inc"))
		);
		Publisher publisher = service.findById(1);
		assertEquals("O'Reilly Media, Inc", publisher.getName());
	}
	
	
	
	

}
