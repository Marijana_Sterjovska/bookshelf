import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import ListBookComponent from './components/ListBookComponent';
import HeaderComponent from './components/HeaderComponent';
import CreateBookComponent from './components/CreateBookComponent';
import UpdateBookComponent from './components/UpdateBookComponent';
import ListAuthorComponent from './components/ListAuthorComponent';
import CreateAuthorComponent from './components/CreateAuthorComponent';
import UpdateAuthorComponent from './components/UpdateAuthorComponent';
import ListPublisherComponent from './components/ListPublisherComponent';
import CreatePublisherComponent from './components/CreatePublisherComponent';
import UpdatePublisherComponent from './components/UpdatePublisherComponent';
import ListCategoryComponent from './components/ListCategoryComponent';
import CreateCategoryComponent from './components/CreateCategoryComponent';
import UpdateCategoryComponent from './components/UpdateCategoryComponent';
import ListFormatComponent from './components/ListFormatComponent';
import CreateFormatComponent from './components/CreateFormatComponent';
import UpdateFormatComponent from './components/UpdateFormatComponent';


function App() {
  return (
    <div>
      <Router>
          <HeaderComponent/>
            <div className="container">
              <Switch> 
                <Route path="/" exact component={ListBookComponent}></Route>
                <Route path="/books" component={ListBookComponent}></Route>
                <Route path="/add-book" component={CreateBookComponent}></Route>
                <Route path="/update-book/:id" component={UpdateBookComponent}></Route>
                </Switch>
              </div>
              <div className="container">
              <Switch> 
                <Route path="/" exact component={ListAuthorComponent}></Route>
                <Route path="/authors" component={ListAuthorComponent}></Route>
                <Route path="/add-author" component={CreateAuthorComponent}></Route>
                <Route path="/update-author/:id" component={UpdateAuthorComponent}></Route>
              </Switch>
              </div>
              <div className="container">
              <Switch> 
                <Route path="/" exact component={ListPublisherComponent}></Route>
                <Route path="/publishers" component={ListPublisherComponent}></Route>
                <Route path="/add-publisher" component={CreatePublisherComponent}></Route>
                <Route path="/update-publisher/:id" component={UpdatePublisherComponent}></Route>
              </Switch>
              </div>
              <div className="container">
              <Switch>           
                <Route path="/" exact component={ListCategoryComponent}></Route>
                <Route path="/categories" component={ListCategoryComponent}></Route>
                <Route path="/add-category" component={CreateCategoryComponent}></Route>
                <Route path="/update-category/:id" component={UpdateCategoryComponent}></Route>
              </Switch>
              </div>
              <div className="container">
              <Switch> 
                <Route path="/" exact component={ListFormatComponent}></Route>
                <Route path="/formats" component={ListFormatComponent}></Route>
                <Route path="/add-format" component={CreateFormatComponent}></Route>
                <Route path="/update-format/:id" component={UpdateFormatComponent}></Route>
              </Switch>
            </div>
      </Router>
    </div>
  );
}

export default App;
