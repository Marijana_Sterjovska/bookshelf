import axios from 'axios';
import React, { Component } from 'react';

class CreateFormatComponent extends Component {
   constructor(props){
        super(props)

        this.state = {
            type : ''
        }
        this.changeTypeHandler= this.changeTypeHandler.bind(this);
        this.saveFormat=this.saveFormat.bind(this);

    }

    saveFormat = (e) => {
        e.preventDefault();
        let format= {type: this.state.type};
        console.log(format);
        
    axios.post('http://localhost:8086/api/formats', format)
        .then (res=> {
            console.log(res);
        });
    }

    changeTypeHandler= (event) => {
        this.setState({type: event.target.value})
    } 

    cancel(){
        this.props.history.push('/formats');
    }

    render() { 
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card-col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Format</h3>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Type:</label>
                                        <input placeholder="Type" name ="type" className="form-control" value={this.state.type} onChange={this.changeTypeHandler}/>
                                    </div>
                                    <button className="btn btn-success" onClick={this.saveFormat}>Save Format</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft:"10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default CreateFormatComponent;