import axios from 'axios';
import React, { Component } from 'react';

const Format = props => (
    <tr>
        <td>{props.format.type}</td>
    </tr>
)
class ListFormatComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            formats:[]
        }
        this.addFormat=this.addFormat.bind(this);
        this.editFormat=this.editFormat.bind(this);
    }

    componentDidMount(){
        axios.get("http://localhost:8086/api/formats")
            .then(response => {
                this.setState({ formats: response.data })
                console.log(response.data);
            })
    }

    formatsList() {
        return this.state.formats.map(currentformat => {
            return <Format format ={currentformat} key={currentformat.id} />;
        })
    }

    editFormat(id){
        this.props.history.push('/update-format/${id}');
    }

    addFormat(){
        this.props.history.push('/add-format');
    }
    
    render() {
        return (
            <div>
                <h2 className="text-center">Format List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addFormat}>Add Format</button>
                </div>
                <div className="row">
                    <table className="table table-striped table-bordered">

                        <thead>
                            <tr>
                                <th>Format type</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.formatsList()
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListFormatComponent;