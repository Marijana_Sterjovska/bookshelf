import axios from 'axios';
import React, { Component } from 'react';

class CreateAuthorComponent extends Component {
   constructor(props){
        super(props)

        this.state = {
            firstName : '',
            lastName : ''
        }
        this.changeFirstNameHandler= this.changeFirstNameHandler.bind(this);
        this.changeLastNameHandler= this.changeLastNameHandler.bind(this);
        this.saveAuthor=this.saveAuthor.bind(this);
    }

    saveAuthor = (e) => {
        e.preventDefault();
        let author= {firstName: this.state.firstName, lastName: this.state.lastName };
        console.log(author);
    
        axios.post('http://localhost:8086/api/authors', author)
        .then (res=> {
            console.log(res);
        });
    }

    changeFirstNameHandler= (event) => {
        this.setState({firstName: event.target.value})
    } 

    changeLastNameHandler= (event) => {
        this.setState({lastName: event.target.value})
    } 

    cancel(){
        this.props.history.push('/authors');
    }

    render() { 
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card-col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Author</h3>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>First name:</label>
                                        <input placeholder="First Name" name ="firstName" className="form-control" value={this.state.firstName} onChange={this.changeFirstNameHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Last Name:</label>
                                        <input placeholder="Last Name" name ="lastName" className="form-control" value={this.state.lastName} onChange={this.changeLastNameHandler}/>
                                    </div>
                                    <button className="btn btn-success" onClick={this.saveAuthor}>Save Author</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft:"10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default CreateAuthorComponent;
