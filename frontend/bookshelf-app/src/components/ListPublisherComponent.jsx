import axios from 'axios';
import React, { Component } from 'react';

const Publisher = props => (
    <tr>
        <td>{props.publisher.name}</td>
    </tr>
)
class ListPublisherComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            publishers:[]
        }
        this.addPublisher=this.addPublisher.bind(this);
        this.editPublisher=this.editPublisher.bind(this);
    }

    componentDidMount(){
        axios.get("http://localhost:8086/api/publishers")
            .then(response => {
                this.setState({ publishers: response.data })
                console.log(response.data);
            })
    }

    publishersList() {
        return this.state.publishers.map(currentpublisher => {
            return <Publisher publisher ={currentpublisher} key={currentpublisher.id} />;
        })
    }

    editPublisher(id){
        this.props.history.push('/update-publisher/${id}');
    }

    addPublisher(){
        this.props.history.push('/add-publisher');
    }
    
    render() {
        return (
            <div>
                <h2 className="text-center">Publisher List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addPublisher}>Add Publisher</button>
                </div>
                <div className="row">
                    <table className="table table-striped table-bordered">

                        <thead>
                            <tr>
                                <th>Publisher name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.publishersList()
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListPublisherComponent;