import axios from 'axios';
import React, { Component } from 'react';

class CreatePublisherComponent extends Component {
   constructor(props){
        super(props)

        this.state = {
            name : ''
        }
        this.changeNameHandler= this.changeNameHandler.bind(this);
        this.savePublisher=this.savePublisher.bind(this);

    }

    savePublisher = (e) => {
        e.preventDefault();
        let publisher= {name: this.state.name};
        console.log(publisher);
        
    axios.post('http://localhost:8086/api/publishers', publisher)
        .then (res=> {
            console.log(res);
        });
    }

    changeNameHandler= (event) => {
        this.setState({name: event.target.value})
    } 

    cancel(){
        this.props.history.push('/publishers');
    }

    render() { 
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card-col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Publisher</h3>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Name:</label>
                                        <input placeholder="Name" name ="name" className="form-control" value={this.state.name} onChange={this.changeNameHandler}/>
                                    </div>
                                    <button className="btn btn-success" onClick={this.savePublisher}>Save Publisher</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft:"10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default CreatePublisherComponent;
