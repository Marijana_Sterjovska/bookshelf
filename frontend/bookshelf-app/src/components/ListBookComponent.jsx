import axios from 'axios';
import React, { Component } from 'react';
import BookService from '../services/BookService';
const Book = props => (
    <tr>
        <td>{props.book.title}</td>
        <td>{props.book.datePublished}</td>
        <td>{props.book.edition}</td>
        <td>{props.book.isbn}</td>
        <td>{props.book.noPages}</td>
        <td>authors</td>
        <td>{props.book.publisher.name}</td>
        <td>{props.book.category.category}</td>
    </tr>
)

class ListBookComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            books:[]
        }
        this.addBook=this.addBook.bind(this);
        this.editBook=this.editBook.bind(this);
    }
    componentDidMount(){
        axios.get("http://localhost:8086/api/books")
            .then(response => {
                this.setState({ books: response.data })
                console.log(response.data);
            })
    }
    booksList() {
        return this.state.books.map(currentbook => {
            return <Book book={currentbook} key={currentbook.id} />;
        })
    }
    editBook(id){
        this.props.history.push('/update-book/${id}');
    }


    addBook(){
        this.props.history.push('/add-book');
    }
    
    render() {
        return (
            <div>
                <h2 className="text-center">Book List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addBook}>Add Book</button>
                </div>
                <div className="row">
                    <table className="table table-striped table-bordered">

                        <thead>
                            <tr>
                                <th>Book title</th>
                                <th>Date published</th>
                                <th>Book edition</th>
                                <th>Book isbn</th>
                                <th>Number of pages</th>
                                <th>Authors</th>
                                <th>Publisher</th>
                                <th>Book category</th>
                                <th>Book formats</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.booksList()
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListBookComponent;