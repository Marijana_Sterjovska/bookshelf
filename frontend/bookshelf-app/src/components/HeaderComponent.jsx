import React, { Component } from 'react';

class HeaderComponent extends Component {
      render() {
        return (
            <div>
                <header>
                    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
                        <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="http://localhost:3000">Bookshelf App</a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" href="http://localhost:3000/books">Books List</a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" href="http://localhost:3000/authors">Authors List</a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" href="http://localhost:3000/publishers">Publishers List</a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" href="http://localhost:3000/categories">Categories List</a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" href="http://localhost:3000/formats">Formats List</a>
                            </li>
                        </ul>
                    </nav>

                 </header>   
            </div>
        );
    }
}

export default HeaderComponent;