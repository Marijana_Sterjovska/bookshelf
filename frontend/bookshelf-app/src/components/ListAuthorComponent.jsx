import axios from 'axios';
import React, { Component } from 'react';

const Author = props => (
    <tr>
        <td>{props.author.firstName}</td>
        <td>{props.author.lastName}</td>
    </tr>
)

class ListAuthorComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            authors:[]
        }
        this.addAuthor=this.addAuthor.bind(this);
        this.editAuthor=this.editAuthor.bind(this);
    }
 
 
    componentDidMount(){
        axios.get("http://localhost:8086/api/authors")
            .then(response => {
                this.setState({ authors: response.data })
                console.log(response.data);
            })
    }


    authorsList() {
        return this.state.authors.map(currentauthor => {
            return <Author author ={currentauthor} key={currentauthor.id} />;
        })
    }

    editAuthor(id){
        this.props.history.push('/update-author/${id}');
    }

   

    addAuthor(){
        this.props.history.push('/add-author');
    }
    
    render() {
        return (
            <div>
                <h2 className="text-center">Author List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addAuthor}>Add Author</button>
                </div>
                <div className="row">
                    <table className="table table-striped table-bordered">

                        <thead>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                  this.authorsList()
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListAuthorComponent;