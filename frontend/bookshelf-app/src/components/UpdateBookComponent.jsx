import React, { Component } from 'react';
import BookService from '../services/BookService';

class UpdateBookComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            id: this.props.match.params.id,
            title : '',
            datePublished :'',
            edition :'',
            isbn :'',
            noPages :'',
            authors :'',
            publisher :'',
            category :'',
            formats :''   
        }
        this.changeTitleHandler= this.changeTitleHandler.bind(this);
        this.changeDatePublishedHandler=this.changeDatePublishedHandler.bind(this);
        this.changeEditionHandler=this.changeEditionHandler.bind(this);
        this.changeIsbnHandler=this.changeIsbnHandler.bind(this);
        this.changeNumberOfPagesHandler=this.changeNumberOfPagesHandler.bind(this);
        this.changeAuthorsHandler=this.changeAuthorsHandler.bind(this);
        this.changePublisherHandler=this.changePublisherHandler.bind(this);
        this.changeCategoryHandler=this.changeCategoryHandler.bind(this);
        this.changeFormatsHandler=this.changeFormatsHandler.bind(this);
        this.updateBook=this.updateBook.bind(this);

    }

    componentDidMount(){
        BookService.getBookById(this.state.id).then((res) =>{
            let book = res.data;
            this.setState({title: book.title, datePublished: book.datePublished, edition: book.edition, isbn: book.isbn,
                noPages: book.noPages, authors: book.authors, publisher: book.publisher, category: book.category, formats: book.formats
            });
     });
    }

    updateBook = (e) => {
        e.preventDefault();
        let book = {title: this.state.title, datePublished: this.state.datePublished, edition: this.state.edition, isbn: this.state.edition, noPages: this.state.noPages, authors:this.state.authors, publisher: this.state.publisher, category: this.state.category, formats: this.state.formats};
        console.log('book => ' + JSON.stringify(book));
        BookService.updateBook(book, this.state.id).then( res =>{
            this.props.history.push('/books');
        });
    }

    changeTitleHandler= (event) => {
        this.setState({title: event.target.value})
    } 
    changeDatePublishedHandler= (event) => {
        this.setState({datePublished: event.target.value})
    } 
    changeEditionHandler= (event) => {
        this.setState({edition: event.target.value})
    } 
    changeIsbnHandler= (event) => {
        this.setState({edition: event.target.value})
    } 
    changeNumberOfPagesHandler= (event) => {
        this.setState({edition: event.target.value})
    } 
    changeAuthorsHandler= (event) => {
        this.setState({edition: event.target.value})
    } 
    changePublisherHandler= (event) => {
        this.setState({edition: event.target.value})
    } 
    changeCategoryHandler= (event) => {
        this.setState({edition: event.target.value})
    } 
    changeFormatsHandler= (event) => {
        this.setState({edition: event.target.value})
    } 

    cancel(){
        this.props.history.push('/books');
    }

    render() { 
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card-col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Book</h3>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Title:</label>
                                        <input placeholder="Title" name ="title" className="form-control" value={this.state.title} onChange={this.changeTitleHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Date Published:</label>
                                        <input placeholder="Date Published" name="datePublished" className="form-control" value={this.state.datePublished} onChange={this.changeDatePublishedHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Edition:</label>
                                        <input placeholder="Edition" name="edition" className="form-control" value={this.state.edition} onChange={this.changeEditionHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Isbn:</label>
                                        <input placeholder="Isbn" name="isbn" className="form-control"  value={this.state.isbn} onChange={this.changeIsbnHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Number of Pages:</label>
                                        <input placeholder="Number of pages" name="noPages" className="form-control" value={this.state.noPages} onChange={this.changeNumberOfPagesHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Authors:</label>
                                        <input placeholder="Authors" name="authors" className="form-control" value={this.state.authors} onChange={this.changeAuthorsHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Publisher:</label>
                                        <input placeholder="Publisher" name="publisher" className="form-control" value={this.state.publisher} onChange={this.changePublisherHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Category:</label>
                                        <input placeholder="Category" name="category" className="form-control" value={this.state.category} onChange={this.changeCategoryHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Formats:</label>
                                        <input placeholder="Formats" name="formats" className="form-control" value={this.state.formats} onChange={this.changeFormatsHandler}/>
                                    </div>

                                    <button className="btn btn-success" onClick={this.updateBook}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft:"10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default UpdateBookComponent;