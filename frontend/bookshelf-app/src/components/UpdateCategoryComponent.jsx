import React, { Component } from 'react';
import CategoryService from '../services/CategoryService';

class UpdateCategoryComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            id: this.props.match.params.id,
            category : ''
        }
        this.changeCategoryHandler= this.changeCategoryHandler.bind(this);
        this.updateCategory=this.updateCategory.bind(this);
    }

    componentDidMount(){
        CategoryService.getCategoryById(this.state.id).then((res) =>{
            let category = res.data;
            this.setState({category: category.category});
     });
    }

    updateCategory = (e) => {
        e.preventDefault();
        let category = {category: this.state.category};
        console.log('category => ' + JSON.stringify(category));
        CategoryService.updateCategory(category, this.state.id).then( res =>{
            this.props.history.push('/categories');
        });
    }

    changeCategoryHandler= (event) => {
        this.setState({category: event.target.value})
    } 
    cancel(){
        this.props.history.push('/categories');
    }

    render() { 
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card-col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Publisher</h3>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Category:</label>
                                        <input placeholder="Category" name ="category" className="form-control" value={this.state.category} onChange={this.changeNameHandler}/>
                                    </div>
                                    
                                    <button className="btn btn-success" onClick={this.updateCategory}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft:"10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default UpdateCategoryComponent;