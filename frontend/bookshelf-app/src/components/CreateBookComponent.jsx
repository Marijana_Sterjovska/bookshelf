import axios from 'axios';
import React, { Component } from 'react';

class CreateBookComponent extends Component {
   constructor(props){
        super(props)

        this.state = {
            title : '',
            datePublished :'',
            edition :'',
            isbn :'',
            noPages :'',
            authors :[],
            publisher :{
                name: ''
            },
            category :{
                category: ''
            },
            formats :[]   
        }
        this.changeTitleHandler= this.changeTitleHandler.bind(this);
        this.changeDatePublishedHandler=this.changeDatePublishedHandler.bind(this);
        this.changeEditionHandler=this.changeEditionHandler.bind(this);
        this.changeIsbnHandler=this.changeIsbnHandler.bind(this);
        this.changeNumberOfPagesHandler=this.changeNumberOfPagesHandler.bind(this);
        this.changeAuthorsFirstNameHandler=this.changeAuthorsFirstNameHandler.bind(this);
        this.changeAuthorsLastNameHandler=this.changeAuthorsLastNameHandler.bind(this);
        this.changePublisherHandler=this.changePublisherHandler.bind(this);
        this.changeCategoryHandler=this.changeCategoryHandler.bind(this);
        this.changeFormatsHandler=this.changeFormatsHandler.bind(this);
        this.saveBook=this.saveBook.bind(this);

    }

    saveBook = (e) => {
        e.preventDefault();
        let book = {title: this.state.title, datePublished: this.state.datePublished, edition: this.state.edition, isbn: this.state.edition, noPages: parseInt(this.state.noPages), authors:this.state.authors, publisher: this.state.publisher, category: this.state.category, formats: this.state.formats};
        console.log(book);
        
    axios.post('http://localhost:8086/api/books', book)
        .then (res=> {
            console.log(res);
        });
    }

    changeTitleHandler= (event) => {
        this.setState({title: event.target.value})
    } 
    changeDatePublishedHandler= (event) => {
        this.setState({datePublished: event.target.value})
    } 
    changeEditionHandler= (event) => {
        this.setState({edition: event.target.value})
    } 
    changeIsbnHandler= (event) => {
        this.setState({isbn: event.target.value})
    } 
    changeNumberOfPagesHandler= (event) => {
        this.setState({noPages: event.target.value})
    } 
    changeAuthorsFirstNameHandler(e) {
        this.setState(prevState => {
            let authors = Object.assign({}, prevState.authors);
            authors.firstName = e.target.value;
            return { authors };
        })
    }
    changeAuthorsLastNameHandler(e) {
        this.setState(prevState => {
            let authors = Object.assign([], prevState.authors);
            authors.lastName = e.target.value;
            return { authors };
        })
    }
    
    changePublisherHandler= (event) => {
        this.setState(prevState => {
            let publisher = Object.assign({}, prevState.publisher);
            publisher.name = event.target.value;
            return { publisher };
        })
    } 
    changeCategoryHandler= (event) => {
        this.setState(prevState => {
            let category = Object.assign({}, prevState.category);
            category.category = event.target.value;
            return { category };
        })
    } 
    changeFormatsHandler= (event) => {
        this.setState(prevState => {
            let formats = Object.assign([], prevState.formats);
            formats.type = event.target.value;            
            return { formats };
        })
    }

    cancel(){
        this.props.history.push('/books');
    }

    render() { 
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card-col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Book</h3>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Title:</label>
                                        <input placeholder="Title" name ="title" className="form-control" value={this.state.title} onChange={this.changeTitleHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Date Published:</label>
                                        <input placeholder="Date Published" name="datePublished" className="form-control" value={this.state.datePublished} onChange={this.changeDatePublishedHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Edition:</label>
                                        <input placeholder="Edition" name="edition" className="form-control" value={this.state.edition} onChange={this.changeEditionHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Isbn:</label>
                                        <input placeholder="Isbn" name="isbn" className="form-control"  value={this.state.isbn} onChange={this.changeIsbnHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Number of Pages:</label>
                                        <input placeholder="Number of pages" name="noPages" className="form-control" value={this.state.noPages} onChange={this.changeNumberOfPagesHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Authors First name:</label>
                                        <input placeholder="Authors" name="authors" className="form-control" value={this.state.authors.firstName} onChange={this.changeAuthorsFirstNameHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Authors Last name:</label>
                                        <input placeholder="Authors" name="authors" className="form-control" value={this.state.authors.lastName} onChange={this.changeAuthorsLastNameHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Publisher:</label>
                                        <input placeholder="Publisher" name="publisher" className="form-control" value={this.state.publisher.name} onChange={this.changePublisherHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Category:</label>
                                        <input placeholder="Category" name="category" className="form-control" value={this.state.category.category} onChange={this.changeCategoryHandler}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Formats:</label>
                                        <input placeholder="Formats" name="formats" className="form-control" value={this.state.formats.type} onChange={this.changeFormatsHandler}/>
                                    </div>

                                    <button className="btn btn-success" onClick={this.saveBook}>Save Book</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft:"10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default CreateBookComponent;
