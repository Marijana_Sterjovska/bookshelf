import React, { Component } from 'react';
import FormatService from '../services/FormatService';

class UpdateFormatComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            id: this.props.match.params.id,
            type : ''  
        }
        this.changeTypeHandler= this.changeTypeHandler.bind(this);
        this.updateFormat=this.updateFormat.bind(this);

    }

    componentDidMount(){
        FormatService.getFormatById(this.state.id).then((res) =>{
            let format = res.data;
            this.setState({type: format.type});
     });
    }

    updateFormat = (e) => {
        e.preventDefault();
        let format = {type: this.state.type};
        console.log('format => ' + JSON.stringify(format));
        FormatService.updateFormat(format, this.state.id).then( res =>{
            this.props.history.push('/formats');
        });
    }

    changeTypeHandler= (event) => {
        this.setState({type: event.target.value})
    } 
    
    cancel(){
        this.props.history.push('/formats');
    }

    render() { 
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card-col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Book</h3>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Type:</label>
                                        <input placeholder="Type" name ="type" className="form-control" value={this.state.type} onChange={this.changeTypeHandler}/>
                                    </div>
                                    <button className="btn btn-success" onClick={this.updateFormat}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft:"10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default UpdateFormatComponent;