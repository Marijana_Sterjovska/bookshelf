import axios from 'axios';
import React, { Component } from 'react';

class CreateCategoryComponent extends Component {
   constructor(props){
        super(props)

        this.state = {
            category : ''
        }
        this.changeCategoryHandler= this.changeCategoryHandler.bind(this);
        this.saveCategory=this.saveCategory.bind(this);

    }

    saveCategory = (e) => {
        e.preventDefault();
        let category= {category: this.state.category};
        console.log(category);

        axios.post('http://localhost:8086/api/categories', category)
        .then (res=> {
            console.log(res);
        });
    }
  
    changeCategoryHandler= (event) => {
        this.setState({category: event.target.value})
    } 

    cancel(){
        this.props.history.push('/categories');
    }

    render() { 
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="card-col-md-6 offset-md-3 offset-md-3">
                            <h3 className="text-center">Add Category</h3>
                            <div className="card-body">
                                <form>
                                    <div className="form-group">
                                        <label>Category:</label>
                                        <input placeholder="Category" name ="category" className="form-control" value={this.state.category} onChange={this.changeCategoryHandler}/>
                                    </div>
                                    <button className="btn btn-success" onClick={this.saveCategory}>Save Category</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft:"10px"}}>Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default CreateCategoryComponent;
