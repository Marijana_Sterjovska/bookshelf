import axios from 'axios';
import React, { Component } from 'react';

const Category = props => (
    <tr>
        <td>{props.category.category}</td>
    </tr>
)
class ListCategoryComponent extends Component {
    constructor(props){
        super(props)

        this.state = {
            categories:[]
        }
        this.addCategory=this.addCategory.bind(this);
        this.editCategory=this.editCategory.bind(this);
    }

    componentDidMount(){
        axios.get("http://localhost:8086/api/categories")
            .then(response => {
                this.setState({ categories: response.data })
                console.log(response.data);
            })
    }


    categoryList() {
        return this.state.categories.map(currentcategory => {
            return <Category category ={currentcategory} key={currentcategory.id} />;
        })
    }

    editCategory(id){
        this.props.history.push('/update-category/${id}');
    }

    addCategory(){
        this.props.history.push('/add-category');
    }
    
    render() {
        return (
            <div>
                <h2 className="text-center">Category List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addCategory}>Add Category</button>
                </div>
                <div className="row">
                    <table className="table table-striped table-bordered">

                        <thead>
                            <tr>
                                <th>Category</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.categoryList()                                        
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default ListCategoryComponent;