import axios from 'axios';

const PUBLISHER_API_BASE_URL="http://localhost:8086/api/publishers";

class PublisherService{

getPublisher(){
    return axios.get(PUBLISHER_API_BASE_URL);
}

createPublisher(publisher){
    return axios.post(PUBLISHER_API_BASE_URL);
}

getPublisherById(publisherId){
    return axios.post(PUBLISHER_API_BASE_URL + '/' + publisherId);
}
updateBook(publisher,publisherId){
    return axios.put(PUBLISHER_API_BASE_URL + "/" + publisherId, publisher);
}

}

export default new PublisherService()