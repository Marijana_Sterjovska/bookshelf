import axios from 'axios';

const AUTHORS_API_BASE_URL="http://localhost:8086/api/authors";

class AuthorService{

getAuthor(){
    return axios.get(AUTHORS_API_BASE_URL);
}

createAuthor(author){
    return axios.post(AUTHORS_API_BASE_URL);
}

getAuthorById(authorId){
    return axios.post(AUTHORS_API_BASE_URL + '/' + authorId);
}
updateAuthor(author,authorId){
    return axios.put(AUTHORS_API_BASE_URL + "/" + authorId, author);
}

}

export default new AuthorService()