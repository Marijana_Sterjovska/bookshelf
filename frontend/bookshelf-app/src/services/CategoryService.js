import axios from 'axios';

const CATEGORY_API_BASE_URL="http://localhost:8086/api/categories";

class CategoryService{

getCategory(){
    return axios.get(CATEGORY_API_BASE_URL);
}

createCategory(category){
    return axios.post(CATEGORY_API_BASE_URL);
}

getCategoryById(categoryId){
    return axios.post(CATEGORY_API_BASE_URL + '/' + categoryId);
}
updateCategory(category,categoryId){
    return axios.put(CATEGORY_API_BASE_URL + "/" + categoryId, category);
}
}

export default new CategoryService()