import axios from 'axios';

const FORMAT_API_BASE_URL="http://localhost:8086/api/formats";

class FormatService{

getFormats(){
    return axios.get(FORMAT_API_BASE_URL);
}

createFormat(format){
    return axios.post(FORMAT_API_BASE_URL,format);
}

getFormatById(formatId){
    return axios.post(FORMAT_API_BASE_URL + '/' + formatId);
}
updateFormat(format,formatId){
    return axios.put(FORMAT_API_BASE_URL + "/" + formatId, format);
}
}

export default new FormatService()