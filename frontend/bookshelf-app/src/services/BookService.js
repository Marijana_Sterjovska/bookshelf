import axios from 'axios';

const BOOK_API_BASE_URL="http://localhost:8086/api/books";

class BookService{

getBooks(){
    return axios.get(BOOK_API_BASE_URL);
}

createBook(book){
    return axios.post(BOOK_API_BASE_URL,book);
}

getBookById(bookId){
    return axios.post(BOOK_API_BASE_URL + '/' + bookId);
}
updateBook(book,bookId){
    return axios.put(BOOK_API_BASE_URL + "/" + bookId, book);
}

}

export default new BookService()